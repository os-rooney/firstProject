public class Arrays {
    public static void main(String[] args) {
        // Durchlauf
        int[] numbers = new int[] {1,2,3,4,5,6,7,8,9};
        durchlauf(numbers);

        // Summe, Maximum und Minimum
        int[] input = new int[] {1, 4, 5, 7, 20000, -511, 100, -200, 400};
        summeMaxMin(input);

        // Ein-Mal-Eins (Bonusaufgabe)
        int[][] array2D = new int[11][11];
        tabelleAuflisten(array2D);

    }
    // Durchlauf
    public static void durchlauf(int[] numbers){
        for(int number:numbers){
            System.out.println(10 - number);
        }
    }

    // Summe
    public static void summeMaxMin(int[] input){
        int summe = 0;
        int min = input[0];
        int max = input[0];

        for(int i = 0; i <= input.length -1; i++){
            // bei jedem Durchlauf die Zahl in Summe aufaddieren
            summe += input[i];
            // überprüfen, ob die aktuelle Zahl kleiner als die in min gespeicherte Zahl ist.
            if(min > input[i]){
                min = input[i];
            }
            // überprüfen, ob die aktuelle Zahl kleiner als die in max gespeicherte Zahl ist.
            if(max < input[i]){
                max = input[i];
            }
        }
        System.out.printf("Die Summe: %d\nDas Minimum: %d\nDas Maximum: %d\n", summe, min, max);
    }

    public static void tabelleAuflisten(int[][] input){
        // Werte zuweisen, für die erste Zeile und Spalte
        for (int zeile = 0; zeile < input.length; zeile++){
            for(int spalte = 0; spalte < input.length; spalte++){
                // Überspringen, falls eine Zeile o. Spalte 0 enthält
                if( zeile == 0 || spalte == 0){
                    continue;
                } else {
                    // das Array initialisieren (Position und Wert)
                    input[zeile][spalte] = zeile * spalte;
                    // Ausgabe
                    System.out.printf("%d \t", input[zeile][spalte]);
                }
            }
            // Neue Zeile
            System.out.printf("\n");
        }
    }
}
